//
// This file is part of the �OS++ III distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_cortex.h"

// ----------------------------------------------------------------------------

// The external clock frequency is specified as a preprocessor definition
// passed to the compiler via a command line option (see the 'C/C++ General' ->
// 'Paths and Symbols' -> the 'Symbols' tab, if you want to change it).
// The value selected during project creation was HSE_VALUE=8000000.
//
// The code to set the clock is at the end.
//
// Note1: The default clock settings assume that the HSE_VALUE is a multiple
// of 1MHz, and try to reach the maximum speed available for the
// board. It does NOT guarantee that the required USB clock of 48MHz is
// available. If you need this, please update the settings of PLL_M, PLL_N,
// PLL_P, PLL_Q to match your needs.
//
// Note2: The external memory controllers are not enabled. If needed, you
// have to define DATA_IN_ExtSRAM or DATA_IN_ExtSDRAM and to configure
// the memory banks in system/src/cmsis/system_stm32f4xx.c to match your needs.

// ----------------------------------------------------------------------------

// Forward declarations.

void
__initialize_hardware(void);

void
configure_system_clock(void);

void init_TIM2();
void init_TIM3();
void init_TIM4();

void init_LED();
void init_EXTI();

static void
MX_USART1_UART_Init(void);
static void
MX_USART6_UART_Init(void);

void
HAL_UART_MspInit(UART_HandleTypeDef* huart);

TIM_HandleTypeDef TIM2_Handle;
TIM_HandleTypeDef TIM3_Handle;
TIM_HandleTypeDef TIM4_Handle;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart6;


GPIO_InitTypeDef GPIO_Init_C;
GPIO_InitTypeDef GPIO_Init_WKUP;

// ----------------------------------------------------------------------------

// This is the application hardware initialisation routine,
// redefined to add more inits.
//
// Called early from _start(), right after data & bss init, before
// constructors.
//
// After Reset the Cortex-M processor is in Thread mode,
// priority is Privileged, and the Stack is set to Main.

void
__initialize_hardware(void)
{
  // Call the CSMSIS system initialisation routine.
  SystemInit();

#if defined (__VFP_FP__) && !defined (__SOFTFP__)

  // Enable the Cortex-M4 FPU only when -mfloat-abi=hard.
  // Code taken from Section 7.1, Cortex-M4 TRM (DDI0439C)

  // Set bits 20-23 to enable CP10 and CP11 coprocessor
  SCB->CPACR |= (0xF << 20);

#endif // (__VFP_FP__) && !(__SOFTFP__)

  // Initialise the HAL Library; it must be the first
  // instruction to be executed in the main program.
  HAL_Init();

  // Warning: The HAL always initialises the system timer.
  // For this to work, the default SysTick_Handler must not hang
  // (see system/src/cortexm/exception_handlers.c)

  // Unless explicitly enabled by the application, we prefer
  // to keep the timer interrupts off.
  //HAL_SuspendTick();

  // Enable HSE Oscillator and activate PLL with HSE as source
  configure_system_clock();

  init_LED();
  init_EXTI();

  // Init timer 2,3 and 4
  init_TIM2();
  init_TIM3();
  init_TIM4();
  // Init USART1
  MX_USART1_UART_Init();
  // Init USART6
  MX_USART6_UART_Init();

  HAL_ResumeTick();

}

#if 1

// This is a sample SysTick handler, use it if you need HAL timings.
void __attribute__ ((section(".after_vectors")))
SysTick_Handler(void)
{
	HAL_IncTick();
}

#endif

// ----------------------------------------------------------------------------


/*** System Clock Configuration*/
void
configure_system_clock(void)
{
	  RCC_OscInitTypeDef RCC_OscInitStruct;
	  RCC_ClkInitTypeDef RCC_ClkInitStruct;

	  __PWR_CLK_ENABLE();

	  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

	  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	  RCC_OscInitStruct.HSICalibrationValue = 16;
	  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	  RCC_OscInitStruct.PLL.PLLM = 16;
	  RCC_OscInitStruct.PLL.PLLN = 400;
	  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
	  RCC_OscInitStruct.PLL.PLLQ = 4;
	  HAL_RCC_OscConfig(&RCC_OscInitStruct);

	  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1;
	  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

	  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
	  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
}



/* Timer2 init function */
void init_TIM2(){

	// Enable TIM2 Clock
	__TIM2_CLK_ENABLE();

	// Config TIM2
	TIM2_Handle.Instance = TIM2;
	TIM2_Handle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	TIM2_Handle.Init.CounterMode = TIM_COUNTERMODE_UP;
	TIM2_Handle.Init.Prescaler = 159; //50 ms			 //159; //100 ms
	TIM2_Handle.Init.Period = 62499;
	HAL_TIM_Base_Init(&TIM2_Handle);
	HAL_TIM_Base_Start_IT(&TIM2_Handle);

	// Config Interrupt
	HAL_NVIC_SetPriority(TIM2_IRQn,2,4);
	HAL_NVIC_EnableIRQ(TIM2_IRQn);
}

void init_EXTI(){

	__GPIOA_CLK_ENABLE();

	GPIO_Init_WKUP.Pin=GPIO_PIN_0;
	GPIO_Init_WKUP.Mode=GPIO_MODE_EVT_RISING;
	GPIO_Init_WKUP.Pull=GPIO_NOPULL;
	GPIO_Init_WKUP.Speed=GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_Init_WKUP);

	__HAL_GPIO_EXTI_GENERATE_SWIT(GPIO_PIN_0);

	HAL_NVIC_SetPriority(EXTI0_IRQn,0,0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}


void init_LED(){

	__GPIOC_CLK_ENABLE();

	GPIO_Init_C.Pin=GPIO_PIN_13;
	GPIO_Init_C.Mode=GPIO_MODE_OUTPUT_PP;
	GPIO_Init_C.Pull=GPIO_NOPULL;
	GPIO_Init_C.Speed=GPIO_SPEED_LOW;

	HAL_GPIO_Init(GPIOC, &GPIO_Init_C);
}


/* Timer3 init function */
void init_TIM3(){

	// Enable TIM3 Clock
	__TIM3_CLK_ENABLE();

	// Config TIM3
	TIM3_Handle.Instance = TIM3;
	TIM3_Handle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	TIM3_Handle.Init.CounterMode = TIM_COUNTERMODE_UP;
	TIM3_Handle.Init.Prescaler = 318; //50 ms			 //159; //100 ms 318~200ms
	TIM3_Handle.Init.Period = 62499;
	HAL_TIM_Base_Init(&TIM3_Handle);
	HAL_TIM_Base_Start_IT(&TIM3_Handle);

	// Config Interrupt
	HAL_NVIC_SetPriority(TIM3_IRQn,2,3);
	HAL_NVIC_EnableIRQ(TIM3_IRQn);

}

/* Timer4 init function */
void init_TIM4(){

	// Enable TIM4 Clock
	__TIM4_CLK_ENABLE();

	// Config TIM4
	TIM4_Handle.Instance = TIM4;
	TIM4_Handle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	TIM4_Handle.Init.CounterMode = TIM_COUNTERMODE_UP;
	TIM4_Handle.Init.Prescaler = 1000; //50 ms			 //159; //100 ms //318~200ms
	TIM4_Handle.Init.Period = 62499;
	HAL_TIM_Base_Init(&TIM4_Handle);
	HAL_TIM_Base_Start_IT(&TIM4_Handle);

	// Config Interrupt
	HAL_NVIC_SetPriority(TIM4_IRQn,15,0);
	HAL_NVIC_EnableIRQ(TIM4_IRQn);

}



/* USART1 init function */
void MX_USART1_UART_Init(void)
{
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  HAL_UART_Init(&huart1);
}




/* USART6 init function */
void MX_USART6_UART_Init(void)
{
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 115200;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  HAL_UART_Init(&huart6);
}


void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  if(huart->Instance==USART1){
    /* Peripheral clock enable */
	__GPIOA_CLK_ENABLE();
	__GPIOB_CLK_ENABLE();
    __USART1_CLK_ENABLE();

    /**USART1 GPIO Configuration
    PB6     ------> USART1_TX
    PB7    ------> USART1_RX */

    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    // Enables the receive interrupt handler
    __HAL_UART_ENABLE_IT(huart, UART_IT_RXNE);


    /* Configure the NVIC for USART2 */
    HAL_NVIC_SetPriority(USART1_IRQn,3,2);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
  }


  if(huart->Instance==USART6){
    /* Peripheral clock enable */
	__GPIOC_CLK_ENABLE();
	__USART6_CLK_ENABLE();


    /**USART6 GPIO Configuration
    PC6     ------> USART6_TX
		 PC7     ------> USART6_RX */

    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    // Enables the receive interrupt handler
   // __HAL_UART_ENABLE_IT(huart, UART_IT_RXNE);

    /* Configure the NVIC for USART2 */
    //HAL_NVIC_SetPriority(USART6_IRQn,3,5);
    //HAL_NVIC_EnableIRQ(USART6_IRQn);
  }
}
