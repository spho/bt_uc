//
// This code builds up on the work of Holger Banzhalf
// Copyright (c) 07.09.2015 Holger Banzhaf.
//
// Major edits and expansions done by Christoph Schildknecht

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Dataset.c"
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"

#define isDebugging 0

// Function prototypes
void TIM2_IRQHandler(void);
void USART1_IRQHandler(void);
void TIM3_IRQHandler(void);
void TIM4_IRQHandler(void);
void USART6_IRQHandler(void);
void EXTI0_IRQnHandler(void);
void float2serial(float number_float);
int sendToBluetoothModule(void);
int decodeReceivedData(uint8_t* d);
void sendToASV(void);

// Init
extern UART_HandleTypeDef huart1; // BT
extern UART_HandleTypeDef huart6; // ASV
extern TIM_HandleTypeDef TIM2_Handle; //Send to ASV
extern TIM_HandleTypeDef TIM3_Handle; //Send to BT
extern TIM_HandleTypeDef TIM4_Handle; //LED toggle
extern GPIO_InitTypeDef GPIO_Init_C;

#define receive_length_BT 4
#define receive_buffer_BT 12
char data_receive_BT[receive_buffer_BT];

#define receive_length_ASV 2
#define receive_buffer_ASV 10

struct Dataset data;

int toggleLed = 0;
int hasChanged = 0;

int countTest = 0;

#if isDebugging
volatile int counterForDebugging = 0;
#endif

// Main
int main() {

	data.hasError = 5;
	data.capacitor = 30;
	data.onBoardElectronics = 50.32;
	data.battery = 70.25;

	float2serial(2.1);
	while (1) {

	}

}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	float2serial(2.5);

}

void EXTI0_IRQnHandler(void) {
	if (__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_0) != RESET) {

		float2serial(2.1);
		__HAL_GPIO_EXTI_CLEAR_FLAG(GPIO_PIN_0);
	}

}

// This timer interrupt sends the received data from bluetooth to the ASV via UART
void TIM2_IRQHandler(void) {

	if (__HAL_TIM_GET_FLAG(&TIM2_Handle, TIM_FLAG_UPDATE) != RESET) {
		__HAL_TIM_CLEAR_FLAG(&TIM2_Handle, TIM_FLAG_UPDATE);

		sendToASV();

#if isDebugging
		counterForDebugging = counterForDebugging + 1;
		float2serial(counterForDebugging);
		float2serial((int8_t) data.torqueRight);
		float2serial((int8_t) data.torqueLeft);
#endif
	}

}

void sendToASV(void) {

	char buf[4];
	buf[0] = (uint8_t) data.torqueRight_high;
	buf[1] = (uint8_t) data.torqueRight_low;
	buf[2] = (uint8_t) data.torqueLeft_high;
	buf[3] = (uint8_t) data.torqueLeft_low;
	HAL_UART_Transmit(&huart6, (uint8_t*) buf, 4, 0x2F);

}

void TIM3_IRQHandler(void) {
	if (__HAL_TIM_GET_FLAG(&TIM3_Handle, TIM_FLAG_UPDATE) != RESET) {
		//Send data to the bluetoothmodule
		sendToBluetoothModule();

		__HAL_TIM_CLEAR_FLAG(&TIM3_Handle, TIM_FLAG_UPDATE);
	}

}

void TIM4_IRQHandler(void) {
	if (__HAL_TIM_GET_FLAG(&TIM4_Handle, TIM_FLAG_UPDATE) != RESET) {
		//HAL_GPIO_TogglePin(&GPIO_Init_C, GPIO_PIN_13);
		if (toggleLed == 0) {
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
			toggleLed = 1;
		} else {
			toggleLed = 0;
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
		}
		__HAL_TIM_CLEAR_FLAG(&TIM4_Handle, TIM_FLAG_UPDATE);
	}
	if(hasChanged==0){
		data.torqueLeft_high=0;
		data.torqueLeft_low=0;
		data.torqueRight_high=0;
		data.torqueRight_low=0;
	}
	hasChanged=0;

}

//Receive data from the ASV
//void USART6_IRQHandler(void) {

//check the type of interrupt to make sure we have received some data.
//if (__HAL_UART_GET_FLAG(&huart6, UART_FLAG_RXNE)) {
//char data_receive_ASV[receive_buffer_ASV];
//HAL_UART_Receive(&huart6, (uint8_t*) data_receive_ASV,
//receive_buffer_ASV, 0x00FF);
//decodeReceivedData(data_receive_ASV);
//}

//}

int sendToBluetoothModule(void) {
	int status = 0;

	float floatMemory = 2.45f;
	uint8_t bufferOfFloat[sizeof(float)];
	bufferOfFloat[0]=0;
	bufferOfFloat[1]=0;
	bufferOfFloat[2]=0;
	bufferOfFloat[3]=0;


	//Encode Data
	volatile uint8_t bufferToBT[24];
	bufferToBT[0] = 127;
	bufferToBT[1] = 22;

	floatMemory = data.battery;
	memcpy(bufferOfFloat, &floatMemory, sizeof(float));
	bufferToBT[3] = 5;
	bufferToBT[4] = bufferOfFloat[0];
	bufferToBT[5] = bufferOfFloat[1];
	bufferToBT[6] = bufferOfFloat[2];
	bufferToBT[7] = bufferOfFloat[3];

	floatMemory = data.hasError;
	memcpy(bufferOfFloat, &floatMemory, sizeof(float));
	bufferToBT[8] = 23;
	bufferToBT[9] = bufferOfFloat[0];
	bufferToBT[10] = bufferOfFloat[1];
	bufferToBT[12] = bufferOfFloat[2];
	bufferToBT[12] = bufferOfFloat[3];

	floatMemory = data.onBoardElectronics;
	memcpy(bufferOfFloat, &floatMemory, sizeof(float));
	bufferToBT[13] = 8;
	bufferToBT[14] = bufferOfFloat[0];
	bufferToBT[15] = bufferOfFloat[1];
	bufferToBT[16] = bufferOfFloat[2];
	bufferToBT[17] = bufferOfFloat[3];

	floatMemory = data.capacitor;
	memcpy(bufferOfFloat, &floatMemory, sizeof(float));
	bufferToBT[18] = 6;
	bufferToBT[19] = bufferOfFloat[0];
	bufferToBT[20] = bufferOfFloat[1];
	bufferToBT[21] = bufferOfFloat[2];
	bufferToBT[22] = bufferOfFloat[3];

	bufferToBT[23] = 127;

	//Send
	HAL_UART_Transmit(&huart1, (uint8_t*) bufferToBT, bufferToBT[1] + 1,
			0x000F);

	return status;
}

int decodeReceivedData(uint8_t* d) {
	int status = 0;
	data.torqueLeft_high = d[0];
	data.torqueRight_high = d[1];
	data.battery = d[2];
	data.hasError = d[3];
	return status;
}

void USART1_IRQHandler(void) {

	int data_receive_start = 0;
	//check the type of interrupt to make sure we have received some data.
	if (__HAL_UART_GET_FLAG(&huart1, UART_FLAG_RXNE)) {
		if (HAL_UART_Receive(&huart1, (uint8_t*) data_receive_BT,
		receive_buffer_BT, 0x0005) == HAL_TIMEOUT) {
			__HAL_UART_DISABLE_IT(&huart1, UART_IT_RXNE);
			__HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE);
		} else {

			for (int j = 0; j < receive_buffer_BT - receive_length_BT - 1;
					j++) {
				if ((data_receive_BT[j] == '.')
						&& (data_receive_BT[j + 1 + receive_length_BT] == 10)) {
					//if ((data_receive_BT[j] == '.')) {
					data_receive_start = j;
					hasChanged++;
					data.torqueLeft_low =
							data_receive_BT[data_receive_start + 3];
					data.torqueRight_low = data_receive_BT[data_receive_start
							+ 4];
					data.torqueLeft_high = data_receive_BT[data_receive_start
							+ 1];
					data.torqueRight_high = data_receive_BT[data_receive_start
							+ 2];
					sendToASV();
					break;

				}
			}

		}
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	float2serial(2.02);

}

// function only used for debugging: displays a float on the terminal of e.g. Putty
void float2serial(float number_float) {

	char buf[10];
	int d1;
	float f2;
	int d2;

	if (number_float >= 0) {
		d1 = number_float;			// Get the integer part (678).
		f2 = number_float - d1;	// Get fractional part (678.0123 - 678 = 0.0123).
		d2 = (int) (f2 * 100000);	// Turn into integer (123).
		sprintf(buf, "%d.%05d\n\r", d1, d2);
	} else {
		number_float = -number_float;
		d1 = number_float;
		f2 = number_float - d1;
		d2 = (int) (f2 * 100000);
		sprintf(buf, "-%d.%05d\n\r", d1, d2);
	}

	HAL_UART_Transmit(&huart6, (uint8_t*) buf, strlen(buf), 0x00FF);
}

