/*
 * Dataset.c
 *
 *  Created on: 20.11.2015
 *      Author: Christoph
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Dataset {

	//Speed of Motors
	float speedM1;
	float speedM2;

	//Torques of Motors
	uint8_t torqueLeft_low;
	uint8_t torqueRight_low;
	uint8_t torqueLeft_high;
	uint8_t torqueRight_high;

	//Speed of vehicle and angle of the front joint
	float speedASV;
	float angleASV;

	//Energy values
	float battery;
	float capacitor;
	float pv;
	float onBoardElectronics;
	float powerM1;
	float powerM2;
	float energyflow[100];

	//Sensor values, to be annaunced in more detail later
	float sensor[100];

	//Pathpoints
	// Pathpoint[] pathpoint= new Pathpoint[100];

	//Recording
	// ArrayList<ArrayList<Byte>> recordings = new ArrayList<ArrayList<Byte>>();
	//int bufferFillRate = 0;
	//boolean isRecording = false;
	//boolean isSync = false;

	//Debugmode
	#define nrOfChars 30

	char request[nrOfChars];
	char reply[nrOfChars];

	float inputSpeed;
	float inputSteering;

	//Error
	int hasError;

} Dataset;

